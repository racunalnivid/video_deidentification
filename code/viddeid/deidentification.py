import numpy as np
import cv2

from .face_detection import face_cascade
from .filters import center_to_origin, origin_to_center, freq_to_space
from .filters.space import gaussian
from .processing.shape import pad_to_shape


class HaarFaceDetectingConvolutionalDeidentificator:

    def __init__(self,
                 filter,
                 detector=lambda im: face_cascade.detectMultiScale(im, 1.2, 1),
                 snr=150,
                 detection_frame_extension=10):
        self.filter = filter
        self.detector = detector
        self.snr = snr
        self.ext = detection_frame_extension

    def get_kernel(self, shape):
        ker = center_to_origin(self.filter, shape)
        ker_ft = np.fft.fft2(ker)
        return ker, ker_ft

    def get_kernel_inverse(self, shape, snr=None):
        if snr is None:
            snr = self.snr

        def wiener_inverse(ft, snr):
            if np.isinf(snr):
                return 1 / ft
            ft_conj = np.conj(ft)
            return ft_conj / (ft * ft_conj + 1 / snr)

        ker, ker_ft = self.get_kernel(shape)
        ker_inv_ft = wiener_inverse(ker_ft, snr)
        ker_inv = np.fft.ifft2(ker_inv_ft).real
        return ker_inv, ker_inv_ft

    def convolve(self, im, inverse=False):
        ker, ker_ft = (self.get_kernel_inverse(im.shape)
                       if inverse else self.get_kernel(im.shape))
        im_ft = np.fft.fft2(im)
        im_conv_ft = im_ft * ker_ft
        return np.fft.ifft2(im_conv_ft).real

    def deidentify(self, im, return_detections=False):
        im_conv = self.convolve(im)
        faces = self.detector(im)
        im_deid = np.copy(im)
        e = self.ext
        for (x, y, w, h) in faces:
            im_deid[y - e:y + h + e, x - e:x + w + e] = im_conv[y - e:y + h + e,
                                                                x - e:x + w + e]
        im_deid = im_deid.astype(np.ubyte)
        return (im_deid, faces) if return_detections else im_deid

    def reconstruct(self, im_deid):
        ker, ker_ft = self.get_kernel(im_deid.shape)
        ker_inv, ker_inv_ft = self.get_kernel_inverse(im_deid.shape)
        ker_inv = pad_to_shape(center_to_origin(ker_inv), im_deid.shape)

        im_deid_ft = np.fft.fft2(im_deid)

        im_deid_deconv_ft = ker_inv_ft * im_deid_ft
        im_deid_deconv = np.fft.ifft2(im_deid_deconv_ft).real
        im_deid_deconv = np.clip(im_deid_deconv, 0, 255)

        return im_deid_deconv.astype(np.ubyte)
