import numpy as np
import cv2
import matplotlib as mpl

def get_color_palette(n, black_first=True):
    cmap = mpl.cm.get_cmap('hsv')
    colors = [np.zeros(3)] if black_first else []
    colors += [
        np.array(cmap(0.8 * i / (n - len(colors) - 1))[:3])
        for i in range(n - len(colors))
    ]
    return colors


def fuse_images(im1, im2, a):
    return a * im1 + (1 - a) * im2


def compose(images, format='0,0;1,0-1'):

    def get_image(frc):
        inds = [int(i) for i in frc.split('-')]
        assert (len(inds) <= 2)
        ims = [images[i] for i in inds]
        return ims[0] if len(ims) == 1 else fuse_images(ims[0], ims[1], 0.5)

    format = format.split(';')
    format = [f.split(',') for f in format]
    return np.concatenate([
        np.concatenate([get_image(frc) for frc in frow], 1) for frow in format
    ], 0)


class Viewer:
    """
        Press "q" to close the window. Press anything else to change the displayed
        composite image. Press "a" to return to the previous image.
    """

    def __init__(self, name=None):
        self.name = name

    def display(self, data, mapping=lambda x: x, window_name='image'):
        i = 0

        while True:
            im = mapping(data[i])
            while True:
                cv2.imshow('Viewer: ' + window_name, im)
                key = cv2.waitKey(1000) & 0xFF
                if key in [37, ord('a')]:
                    i -= 1
                    break
                elif key in [39, ord('d')]:
                    i += 1
                    break
                elif key in [ord('q'), 27]:  # 27=ESC
                    cv2.destroyAllWindows()
                    return
            i = i % len(data)
    def display_video(self, frames , window_name ='video'):
        while True:
            for frame in frames:
                cv2.imshow(window_name, frame)
                key = cv2.waitKey(20) & 0xFF
                if key in [ord('q'), 27]:  # 27=ESC
                    cv2.destroyAllWindows()
                    return
