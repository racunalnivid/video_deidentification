import numpy as np
import cv2

import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))  # /*

from viddeid.ioutils import path, directory
from viddeid import visualization
import viddeid.filters.space as sfilters
from viddeid import filters

#CONTROLS#
#=======================#
#Show the initial video on start up
show_initial_video = False
#Show the filtered video only at the end
show_filtered_video = False
#Process all the videos in the test_videos folder
process_all = True
#Process a specific video with index (ignored if process_all is active)
process_index = 0
#=======================#

#Get test videos directory
dir_path = path.find_in_ancestor(__file__, "data/test_videos")
#Save all videos form the video directory
videos = [cv2.VideoCapture(p) for p in directory.get_files(dir_path) ]
#Initialize a  viewer for the processing
viewer = visualization.Viewer()


def process(im):

    def normalize_0_255(x):
        x = np.clip(x, 0, np.inf)
        x *= 255 / np.max(x)
        return x.astype(np.uint8)

    def make_ft_visible(ft):
        ft = np.fft.fftshift(ft)
        ft = np.sqrt(np.abs(ft))
        return normalize_0_255(ft)

    ft_im = np.fft.fft2(im)

    ker = sfilters.gaussian_mixture([
        ((2, 3), np.array([[2, 0], [0, 2]]), 1),
        ((5, -4), np.array([[2, 1], [1, 2]]), 1),
        ((-2, -4), np.array([[2, -1], [-1, 2]]), 1),
    ])
    ker = filters.center_to_origin(ker, im.shape)

    ft_ker = np.fft.fft2(ker)

    ft_conv = ft_im * ft_ker
    im_conv = np.fft.ifft2(ft_conv)  # filtered image

    ft_conv = np.fft.fft2(im_conv)
    ft_im_r = ft_conv / ft_ker

    im_r = np.fft.ifft2(ft_im_r)  # reconstructed image

    ims = [
        im,
        normalize_0_255(filters.origin_to_center(ker)),
        np.clip(im_conv.real, 0, 255).astype(np.uint8),
        im_r.real.astype(np.uint8)
    ]
    fts = [make_ft_visible(a) for a in [ft_im, ft_ker, ft_conv, ft_im_r]]
    return visualization.compose(ims + fts, '0,1,2,3;4,5,6,7')

def filter(images):
    filtered_images =[]
    for im in images:
        ft_im = np.fft.fft2(im)

        ker = sfilters.gaussian_mixture([
            ((2, 3), np.array([[2, 0], [0, 2]]), 1),
            ((5, -4), np.array([[2, 1], [1, 2]]), 1),
            ((-2, -4), np.array([[2, -1], [-1, 2]]), 1),
        ])
        ker = filters.center_to_origin(ker, im.shape)

        ft_ker = np.fft.fft2(ker)

        ft_conv = ft_im * ft_ker
        im_conv = np.fft.ifft2(ft_conv)
        filtered_images.append(np.clip(im_conv.real, 0, 255).astype(np.uint8))
    return filtered_images

if process_all != True:
    videos = [videos[process_index]]
for vidcap in videos:
    #Make an array for the frames and keep count of the current frame
    count = 0
    images = []
    success,image = vidcap.read()
    while success:
        #Convert to grayscale
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        images.append(gray)
        count += 1
        print('Reading frame: ' + count.__str__())
        success, image = vidcap.read()
    print ('Succesfully read video')
    #Show the initial video
    if show_initial_video ==True:
        print('Showing initial video...')
        viewer.display_video(images, 'Initial video')
    #Process the images
    print('Processing images...')
    process_images = list(map((lambda x: process(x)), images))
    print('Processing done.')
    print('Showing processing video...')
    print('Press q or ESC to exit the video')
    viewer.display_video(process_images , 'Process images')
    #Show the filtered video
    if show_filtered_video ==True:
        print('Showing filtered video...')
        viewer.display_video(filter(images), 'Filtered video')






