import numpy as np

from ..processing.shape import pad_to_shape, crop


def center_to_origin(filter, shape=None):
    """ Shifts/rolls the filter by (-h/2, -w/2). """
    if shape is None:
        shape = filter.shape
    else:
        for a, b in zip(filter.shape, shape):
            assert a <= b
        filter = pad_to_shape(filter, shape)
    return np.roll(filter, [-d // 2 for d in shape], axis=[0, 1])


def origin_to_center(filter):
    """ Shifts/rolls the filter by (h/2, w/2). """
    return np.roll(filter, [d // 2 for d in filter.shape], axis=[0, 1])


def space_to_freq(filter, shape=None):
    """ Returns the Fourier transform of filter centered at origin=(0,0). """
    filter = center_to_origin(filter, shape)
    return np.fft.fft2(filter)


def freq_to_space(filter, shape=None):
    """ Returns the filter in the spatial domain centered at origin=(0,0). """
    if shape is not None:
        for a, b in zip(filter.shape, shape):
            assert a >= b
        filter = crop(filter, shape)
    filter = np.fft.ifft2(filter).real
    filter = origin_to_center(filter)
    return filter
