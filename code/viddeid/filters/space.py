import numpy as np
import skimage

# space domain filters


def const_square(ksize, normalized=True):
    filt = np.ones((ksize, ksize), np.float32)
    return filt / np.sum(filt) if normalized else filt


def const_circle(radius, ksize=None, normalized=True):
    assert radius is not None or ksize is not None
    if ksize is not None:
        if radius is None:
            radius = ksize / 2
    else:
        ksize = int(radius * 2 + 0.5)

    center = (ksize - 1) / 2
    radius_sqr = radius**2
    filt = np.zeros((ksize, ksize))
    for i in range(ksize):
        for j in range(ksize):
            filt[i, j] = (center - i)**2 + (center - j)**2 < radius_sqr
    return filt / np.sum(filt) if normalized else filt


def gaussian(sigma=None, ksize=None, normalized=True):
    """ An unnormalized gaussian-like filter with ksize = 8*sigma by default """
    assert sigma is not None or ksize is not None
    if sigma is None:
        sigma = ksize / 8
    elif sigma == 0:
        sigma = 1e-1
    if ksize is None:
        ksize = int(8 * sigma + 0.5)

    center = (ksize - 1) / 2
    gamma = 1 / (2 * sigma**2)
    filt = np.zeros((ksize, ksize))
    for i in range(ksize):
        for j in range(ksize):
            filt[i, j] = np.exp(-gamma * ((center - i)**2 + (center - j)**2))
    return filt / np.sum(filt) if normalized else filt


def gaussian_mixture(pos_cov_weight_tuples):
    positions = np.array([
        np.array([p[0][0], p[0][1]], dtype=np.float)
        for p in pos_cov_weight_tuples
    ])
    s_invs = [np.linalg.inv(p[1]) for p in pos_cov_weight_tuples]
    ws = [p[2] for p in pos_cov_weight_tuples]
    max_xy = np.max(positions, axis=0)
    min_xy = np.min(positions, axis=0)
    positions -= (max_xy + min_xy) / 2

    ksize = int(np.max(max_xy - min_xy) + 0.5)
    get_center = lambda ksize: np.array([(ksize - 1) / 2] * 2)
    center = get_center(ksize)
    filt = np.zeros((ksize, ksize))
    for g in range(len(ws)):
        for i in range(ksize):
            for j in range(ksize):
                r = np.array([i, j]) - positions[g] - center
                filt[i, j] += np.exp(-0.5 * r @ s_invs[g] @ r) * ws[g]
    nonzero = True
    while nonzero:
        nonzero = False
        sumfilt = np.sum(filt) / 256
        ksize += 2
        center = get_center(ksize)
        filt = skimage.util.pad(filt, 1, mode='constant')
        for g in range(len(ws)):
            for i in [0, ksize - 1]:
                for j in range(ksize):
                    r = np.array([i, j]) - positions[g] - center
                    filt[i, j] += np.exp(-0.5 * r @ s_invs[g] @ r) * ws[g]
                    nonzero = nonzero or filt[i, j] / sumfilt >= 0.5
            for i in range(ksize):
                for j in [0, ksize - 1]:
                    r = np.array([i, j]) - positions[g] - center
                    filt[i, j] += np.exp(-0.5 * r @ s_invs[g] @ r) * ws[g]
                    nonzero = nonzero or filt[i, j] / sumfilt >= 0.5
    return filt / np.sum(filt)
