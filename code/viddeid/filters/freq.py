import numpy as np
from skimage import draw

from ..processing.shape import adjust_shape
from skimage.transform import resize
from .space import const_circle

# frequency domain filters


def _adjust_to_shape(filt, shape):
    return resize(adjust_shape(filt, [shape[1]] * 2), shape)


def low_pass(freq, shape):
    return _adjust_to_shape(const_circle(freq + 0.5, normalized=False), shape)


def high_pass(freq, shape):
    return 1 - _adjust_to_shape(const_circle(freq, normalized=False), shape)


def band_pass(freq_low, freq_high, shape):
    return low_pass(freq_high, shape) * high_pass(freq_low, shape)


def band_stop(freq_low, freq_high, shape):
    return 1 - band_pass(freq_low, freq_high, shape)
