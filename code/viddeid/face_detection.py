import cv2

from .ioutils import path

face_cascade = cv2.CascadeClassifier(
    path.find_in_ancestor(
        __file__,
        "data/cascade_classifiers/haarcascade_frontalface_default.xml"))