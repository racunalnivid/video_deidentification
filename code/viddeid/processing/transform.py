import cv2
import numpy as np


def translate(image: np.ndarray, delta: tuple) -> np.ndarray:  # TODO: test
    rows, cols = image.shape
    M = np.float32([[1, 0, delta[0]], [0, 1, delta[1]]])
    return cv2.warpAffine(image, M, (cols, rows), flags=cv2.INTER_NEAREST)


def zoom(image: np.ndarray, proportion: float) -> np.ndarray:  # TODO: test
    rows, cols = image.shape
    M = np.float32([[proportion, 0, 0], [0, proportion, 0]])
    return cv2.warpAffine(image, M, (cols, rows), flags=cv2.INTER_NEAREST)


def rotate(image: np.ndarray, angle: float) -> np.ndarray:  # TODO: test
    rows, cols = image.shape
    M = cv2.getRotationMatrix2D((rows / 2, cols / 2), angle, 1)
    return cv2.warpAffine(image, M, (cols, rows), flags=cv2.INTER_NEAREST)
