import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))  # /*

import numpy as np

import cv2
from viddeid import visualization
from viddeid.ioutils import directory, path

# loading
dir_path = path.find_in_ancestor(__file__, "data/test_images")
images = [
    cv2.imread(p, cv2.IMREAD_GRAYSCALE) for p in directory.get_files(dir_path)
]

# display the image
print("Displaying the image")
print("Use arrow keys or 'a' and 'd' to switch images. Close with 'q' or ESC.")
viewer = visualization.Viewer()
viewer.display(images)  # close with 'q' or ESC
