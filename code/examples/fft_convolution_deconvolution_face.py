import numpy as np
import cv2

import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))  # /*

from viddeid.ioutils import path, directory
from viddeid import visualization
import viddeid.filters.space as sfilters
import viddeid.filters.freq as ffilters
from viddeid import filters

face_cascade = cv2.CascadeClassifier(
    path.find_in_ancestor(
        __file__,
        "data/cascade_classifiers/haarcascade_frontalface_default.xml"))

dir_path = path.find_in_ancestor(__file__, "data/test_images")
images = [
    cv2.imread(p, cv2.IMREAD_GRAYSCALE) for p in directory.get_files(dir_path)
]
viewer = visualization.Viewer()

def process(im):

    def normalize_0_255(x):
        x = np.clip(x, 0, np.inf)
        x *= 255 / np.max(x)
        return x.astype(np.uint8)

    def make_ft_visible(ft):
        ft = np.fft.fftshift(ft)
        ft = np.sqrt(np.abs(ft))
        return normalize_0_255(ft)

    ft_im = np.fft.fft2(im)

    ker = sfilters.gaussian_mixture([
        ((2, 3), np.array([[2, 0], [0, 2]]), 1),
        ((5, -4), np.array([[2, 1], [1, 2]]), 1),
        ((-2, -4), np.array([[2, -1], [-1, 2]]), 1),
    ])
    ker = filters.center_to_origin(ker, im.shape)

    ft_ker = np.fft.fft2(ker)

    ft_conv = ft_im * ft_ker
    im_conv = np.fft.ifft2(ft_conv)  # filtered image

    ft_conv = np.fft.fft2(im_conv)
    ft_im_r = ft_conv # / ft_ker

    im_deid = deid_faces(im, np.clip(im_conv.real, 0, 255).astype(np.uint8))
    im_r = np.fft.ifft2(im_deid)  # reconstructed image


    ims = [
        im,
        normalize_0_255(filters.origin_to_center(ker)),
        im_deid,
        im_r.real.astype(np.uint8)
    ]
    fts = [make_ft_visible(a) for a in [ft_im, ft_ker, ft_conv, ft_im_r]]
    return visualization.compose(ims + fts, '0,1,2,3;4,5,6,7')

def deid_faces(img, deintificateImg):
    faces = face_cascade.detectMultiScale(img, 1.3, 1)
    deintificateFaceImg = np.copy(img)
    for (x, y, w, h) in faces:
        for i in range(x,x+w):
            for j in range(y, y+h):
                deintificateFaceImg[j,i] = deintificateImg[j,i]
    return deintificateFaceImg

viewer.display(images, lambda im: process(im))