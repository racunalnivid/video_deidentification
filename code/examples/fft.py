import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))  # /*

import numpy as np

import cv2
from viddeid import visualization
from viddeid.ioutils import directory, path

dir_path = path.find_in_ancestor(__file__, "data/test_images")
images = [
    cv2.imread(p, cv2.IMREAD_GRAYSCALE) for p in directory.get_files(dir_path)
]

viewer = visualization.Viewer()


def process(im):

    def make_visible(ft):
        ft = np.fft.fftshift(ft)
        ft = np.abs(ft)**0.5
        ft *= 255 / np.max(ft)
        return ft.astype(np.uint8)

    ft = np.fft.fft2(im)
    imr = np.fft.ifft2(ft).real.astype(np.uint8)
    return visualization.compose([im, make_visible(ft), im*0, imr], '0,1;2,3')


viewer.display(images, lambda im: process(im), 'convolution')
