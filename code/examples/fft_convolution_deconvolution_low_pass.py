import numpy as np
import cv2

import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))  # /*

from viddeid.ioutils import path, directory
from viddeid import visualization
import viddeid.filters.space as sfilters
import viddeid.filters.freq as ffilters
from viddeid import filters

dir_path = path.find_in_ancestor(__file__, "data/test_images")
images = [
    cv2.imread(p, cv2.IMREAD_GRAYSCALE) for p in directory.get_files(dir_path)
]

viewer = visualization.Viewer()


def process(im):

    def normalize_0_255(x):
        x = np.clip(x, 0, np.inf)
        x *= 255 / np.max(x)
        return x.astype(np.uint8)

    def make_ft_visible(ft):
        ft = np.fft.fftshift(ft)
        ft = np.sqrt(np.abs(ft))
        return normalize_0_255(ft)

    ft_im = np.fft.fft2(im)

    ft_ker = ffilters.low_pass(30, ft_im.shape)
    ft_ker = filters.center_to_origin(ft_ker)

    ker = np.fft.ifft2(ft_ker)

    ft_conv = ft_im * ft_ker
    im_conv = np.fft.ifft2(ft_conv)  # filtered image

    ft_conv = np.fft.fft2(im_conv)
    ft_im_r = ft_conv / ft_ker

    im_r = np.fft.ifft2(ft_im_r)  # reconstructed image

    ims = [
        im,
        normalize_0_255(filters.origin_to_center(ker)),
        np.clip(im_conv.real, 0, 255).astype(np.uint8),
        im_r.real.astype(np.uint8)
    ]
    fts = [make_ft_visible(a) for a in [ft_im, ft_ker, ft_conv, ft_im_r]]
    return visualization.compose(ims + fts, '0,1,2,3;4,5,6,7')


viewer.display(images, lambda im: process(im), 'convolution')
