import numpy as np
import cv2

import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))  # /*

from viddeid.ioutils import path, directory
from viddeid import visualization
import viddeid.filters.space as sfilters
import viddeid.filters.freq as ffilters
from viddeid import filters

face_cascade = cv2.CascadeClassifier(
    path.find_in_ancestor(
        __file__,
        "data/cascade_classifiers/haarcascade_frontalface_default.xml"))

dir_path = path.find_in_ancestor(__file__, "data/test_images")
images = [cv2.imread(p) for p in directory.get_files(dir_path)]

viewer = visualization.Viewer()

def detect_faces(img):
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(img_gray, 1.3, 1)
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
    return img

viewer.display(images, detect_faces)