import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))  # /*

import numpy as np

import cv2
from viddeid import visualization
from viddeid.filters import center_to_origin, origin_to_center
from viddeid.filters.space import const_circle, const_square, gaussian, gaussian_mixture
from viddeid.ioutils import directory, path

radius = 10
sigma = radius / 4
kernels = [
    gaussian(sigma),
    center_to_origin(gaussian(sigma)),
    const_circle(radius),
    origin_to_center(const_circle(radius)),
    const_square(radius * 2),
    gaussian_mixture([
        ((2, 3), np.array([[2, 0], [0, 2]]), 1),
        ((5, -4), np.array([[2, 1], [2, 2]]), 1),
        ((-2, -4), np.array([[2, -1], [-1, 2]]), 1),
    ]),
]

# display the image
print("Displaying the image")
viewer = visualization.Viewer()
viewer.display(
    list(map(lambda im: im / np.max(im), kernels)))  # close with 'q' or ESC
