import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))  # /*

import numpy as np

import cv2
from viddeid import visualization
from viddeid.ioutils import directory, path

dir_path = path.find_in_ancestor(__file__, "data/test_images")

print("Preprocessing before displaying")
print("In this case images are scalars and the lambda is preprocessign them.")
print("Use arrow keys or 'a' and 'd' to switch images. Close with 'q' or ESC.")
im_path = os.path.join(dir_path, "kokosi.png")
image = cv2.imread(im_path, cv2.IMREAD_GRAYSCALE)
viewer = visualization.Viewer()
viewer.display(np.arange(1, 50), lambda k: (image / k).astype(np.ubyte))
