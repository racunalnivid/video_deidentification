import numpy as np
import cv2

import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))  # /*

from viddeid.ioutils import path, directory
from viddeid import visualization
import viddeid.filters.space as sfilters

im_path = path.find_in_ancestor(__file__, "data/test_images/kokosi.png")
image = cv2.imread(im_path, cv2.IMREAD_GRAYSCALE)

viewer = visualization.Viewer()


def conv_const(im, ksize):
    kernel = sfilters.gaussian(ksize=ksize)
    return cv2.filter2D(im, -1, kernel)


viewer.display(
    np.arange(1, 100, 2), lambda k: conv_const(image, k), 'convolution')
