import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))  # /*

from viddeid.ioutils import path, directory
from viddeid import visualization
import viddeid.filters.space as sfilters
from viddeid import filters
from viddeid.face_detection import face_cascade
from viddeid.deidentification import HaarFaceDetectingConvolutionalDeidentificator

originalPicsDir = "/Users/miko/Desktop/baze lica/faces in the wild/fddb/originalPics/"
FDDBfoldsDir = "/Users/miko/Desktop/baze lica/faces in the wild/fddb/FDDB-folds/"
resultDir = path.find_in_ancestor(__file__, "data/result")
iouTP = 0.5


def detect_faces(img, resultFile):
    faces, rejectLevels, levelWeights = face_cascade.detectMultiScale3(img, 1.2, 1, outputRejectLevels=True)
    resultFile.write(str(len(faces)) + "\n")
    for i in range(0, len(faces)):
        x, y, w, h = faces[i]
        weight = levelWeights[i][0]
        weight = weight/20
        resultFile.write(str(x) + " " + str(y) + " " + str(w) + " " + str(h) + " " + str(weight) + "\n")
    return max

def processDataset():
    real = dict()
    predict = dict()
    for filePath in directory.get_files(FDDBfoldsDir):
        if filePath.endswith("ellipseList.txt"):
            file = open(filePath, "r")
            line = file.readline()[:-1]
            while (line):
                numbOfFaces = int(file.readline()[:-1])
                faces = []
                for i in range(0, numbOfFaces):
                    elements = file.readline()[:-1].split(" ")
                    x_min = float(elements[3]) - float(elements[1])
                    y_min = float(elements[4]) - float(elements[0])
                    if x_min < 0:
                        x_min = 0
                    if y_min < 0:
                        y_min = 0

                    faces.append(({"x1": x_min, "y1": y_min,
                                  "x2": float(elements[3]) + float(elements[1]),
                                  "y2": float(elements[4]) + float(elements[0])}))
                real[line] = faces
                line = file.readline()[:-1]

    for filePath in directory.get_files(resultDir):
        if filePath.endswith("out.txt"):
            file = open(filePath, "r")
            line = file.readline()[:-1]
            while (line):
                numbOfFaces = int(file.readline()[:-1])
                faces = []
                for i in range(0, numbOfFaces):
                    elements = file.readline()[:-1].split(" ")

                    faces.append(({"x1": int(elements[0]),
                                   "y1": int(elements[1]),
                                   "x2": int(elements[0]) + int(elements[2]),
                                   "y2": int(elements[1]) + int(elements[3])}))
                predict[line] = faces
                line = file.readline()[:-1]
    return real, predict


def evaluate(real, predict):
    tpUk = 0
    fpUk = 0
    fnUk = 0
    precisionList = []
    recalList = []
    for filePath in directory.get_files(FDDBfoldsDir):
        tp = 0
        fp = 0
        fn = 0
        if not filePath.endswith("ellipseList.txt"):
            file = open(filePath, "r")
            line = file.readline()[:-1]
            facesPredict = 0
            facesReal = 0
            while (line):
                for objA in real[line]:
                    for objB in predict[line]:
                        if compareBoxes(objA, objB):
                            tp += 1
                            break
                facesPredict += len(real[line])
                facesReal += len(predict[line])
                line = file.readline()[:-1]

            fp += facesPredict - tp
            fn += facesReal - tp
            tpUk += tp
            fpUk += fp
            fnUk += fn

            precision = tp / (tp + fp)
            recal = tp / (tp + fn)
            precisionList.append(precision)
            recalList.append(recal)

    meanPrecision = tpUk / (tpUk + fpUk)
    meanRecall = tpUk / (tpUk + fnUk)
    return meanPrecision, meanRecall, precisionList, recalList


def compareBoxes(bb1, bb2):
    """
    Calculate the Intersection over Union (IoU) of two bounding boxes.

    Parameters
    ----------
    bb1 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x1, y1) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner
    bb2 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x, y) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner

    Returns
    -------
    float
        in [0, 1]
    """
    assert bb1['x1'] < bb1['x2']
    assert bb1['y1'] < bb1['y2']
    assert bb2['x1'] < bb2['x2']
    assert bb2['y1'] < bb2['y2']

    # determine the coordinates of the intersection rectangle
    x_left = max(bb1['x1'], bb2['x1'])
    y_top = max(bb1['y1'], bb2['y1'])
    x_right = min(bb1['x2'], bb2['x2'])
    y_bottom = min(bb1['y2'], bb2['y2'])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1['x2'] - bb1['x1']) * (bb1['y2'] - bb1['y1'])
    bb2_area = (bb2['x2'] - bb2['x1']) * (bb2['y2'] - bb2['y1'])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    if iou >iouTP:
        return True
    return False

real, predict = processDataset()
meanPrecision, meanRecall, precisionList, recalList = evaluate(real, predict)

print("Mean precision:", meanPrecision)
print("Mean recall:", meanRecall)

red_patch = mpatches.Patch(color='red', label='Precision')
blue_patch = mpatches.Patch(color='blue', label='Recall')
plt.legend(handles=[red_patch, blue_patch])
plt.ylabel('Value')
plt.xlabel('Fold')

x = np.arange(1, 11, 1)

# plt.plot(x, precizionValuesTest, '-b', x, precizionValuesTrain, '-r')
plt.plot(x, recalList, '-b', x, precisionList, '-r')
plt.show()