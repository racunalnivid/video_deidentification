import numpy as np
import cv2

import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))  # /*

from viddeid.ioutils import path, directory
from viddeid import visualization
import viddeid.filters.space as sfilters
from viddeid import filters
from viddeid.face_detection import face_cascade
from viddeid.deidentification import HaarFaceDetectingConvolutionalDeidentificator

originalPicsDir = "/Users/miko/Desktop/baze lica/faces in the wild/fddb/originalPics/"
FDDBfoldsDir = "/Users/miko/Desktop/baze lica/faces in the wild/fddb/FDDB-folds/"
resultDir = path.find_in_ancestor(__file__, "data/result")


def detect_faces(img, resultFile):
    faces, rejectLevels, levelWeights = face_cascade.detectMultiScale3(img, 1.225, 1, outputRejectLevels=True)
    resultFile.write(str(len(faces)) + "\n")
    for i in range(0, len(faces)):
        x, y, w, h = faces[i]
        weight = levelWeights[i][0]
        weight = weight/20
        resultFile.write(str(x) + " " + str(y) + " " + str(w) + " " + str(h) + " " + str(weight) + "\n")
    return max

def processDataset():
    for filePath in directory.get_files(FDDBfoldsDir):
        if filePath.endswith("ellipseList.txt"):
            continue
        file = open(filePath, "r")
        numb = filePath.split("/")[-1].split("-")[-1].split(".")[0]

        resultFile = open(resultDir + '/' + "fold-" + numb + "-out.txt", 'w')
        line = file.readline()[:-1]
        while (line):
            imgFile = originalPicsDir + line + ".jpg"
            resultFile.write(line + "\n")
            detect_faces(cv2.imread(imgFile, cv2.IMREAD_GRAYSCALE), resultFile)
            line = file.readline()[:-1]
        resultFile.close()
processDataset()
