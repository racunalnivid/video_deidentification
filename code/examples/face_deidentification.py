import numpy as np
import cv2

import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))  # /*

from viddeid.ioutils import path, directory
from viddeid import visualization
import viddeid.filters.space as sfilters
from viddeid import filters

from viddeid.deidentification import HaarFaceDetectingConvolutionalDeidentificator

dir_path = path.find_in_ancestor(__file__, "data/test_images")
images = [
    cv2.imread(p, cv2.IMREAD_GRAYSCALE) for p in directory.get_files(dir_path)
]
viewer = visualization.Viewer()


def process(im):

    def normalize_0_255(x):
        x = np.clip(x, 0, np.inf)
        x *= 255 / np.max(x)
        return x.astype(np.uint8)

    ker = sfilters.gaussian_mixture([
        ((8, 3), np.array([[4, 2], [2, 3]]) * 4, 0.5),
        ((-5, -7), np.array([[2, 2], [2, 3]]) * 4, 1),
        ((-5, 6), np.array([[5, -2], [-2, 2]]) * 4, 1),
    ])

    # ker = sfilters.gaussian_mixture([
    #     ((10, 1), np.array([[4, 2], [2, 3]]) * 5, 0.5),
    #     ((-5, -10), np.array([[2, 2], [2, 3]]) * 5, 1),
    #     ((-10, 5), np.array([[5, -2], [-2, 2]]) * 5, 1),
    # ])

    deid = HaarFaceDetectingConvolutionalDeidentificator(
        ker, snr=100 if True else np.inf, detection_frame_extension=10)
    im_deid = deid.deidentify(im)
    im_r = deid.reconstruct(im_deid)

    im_deid_conv = deid.convolve(im).astype(np.uint8)
    im_deid_conv += np.random.randn(*im_deid_conv.shape).astype(np.uint8)

    ims = [
        im,
        im_deid,
        im_r,
        normalize_0_255(filters.origin_to_center(deid.get_kernel(im.shape)[0])),
        normalize_0_255(
            filters.origin_to_center(deid.get_kernel_inverse(im.shape)[0])),
        deid.reconstruct(im_deid_conv),
    ]
    return visualization.compose(ims, '0,1,2;3,4,5')


viewer.display(images, lambda im: process(im))